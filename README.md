Radix Sort Presentation
=======================

These are the materials from my Radix Sort Presentation given in CS3851 on 2021-05-12.

## Contents:
* `radix_sort.py` -- Contains a rudimentary python implementation designed to be relatively easy 
  to understand

* `sorting.jl` -- Contains sorting algorithms used for our benchmarks. The radix sort used here is
  much improved, because it uses integer operations and modifies the original array, rather than 
  copying data into buckets

* `benchmarks.jmd` -- The Julia Markdown file used to perform benchmarks. If you're familiar with 
  Rmarkdown, it's basically that, but for Julia. (I preferred that over making a Jupyter notebook. If you really wanted to, you could convert it to Jupyter with Weave.jl)

* `benchmarks.html` -- The output from `benchmarks.jmd`, rendered to html for your viewing pleasure

* `presentation.odp` -- The presentation shown in class. Powerpoint can *probably* open it

* `presentation.pdf` -- A PDF render of the presentation
