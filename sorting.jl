using Pipe

"""
Perform a simple, iterative Radix Sort operation

Note that this very simple Radix Sort will only work on positive integers
"""
function radix_sort!(arr::AbstractVector{T} where T <: Integer, radix=10)
    iterations = @pipe maximum(arr) |> log(radix, _) |> trunc(Int, _)

    for i in 0:iterations
        # Copy array and get digits
        source = arr[:]
        digits = source .÷ (radix^i) .% radix .+ 1

        # Divide Source Array into buckets
        counts = fill(0, radix + 1)
        foreach(x -> counts[x + 1] += 1, digits)
        cumsum!(counts, counts)
        
        # Fill buckets
        indices = fill(1, radix)
        for (digit, value) in zip(digits, source)
            arr[counts[digit] + indices[digit]] = value
            indices[digit] += 1
        end
    end
end

"""
Perform a simple Merge Sort operation
"""
function merge_sort!(arr::AbstractVector{T} where T <: Integer)
    if length(arr) == 1
        return
    end

    mid = length(arr) ÷ 2
    view(arr, 1:mid) |> merge_sort!
    view(arr, (mid + 1):length(arr)) |> merge_sort!

    upper = arr[1:mid]
    lower = arr[mid + 1:end]
    li = ui = 1
    for i in 1:length(arr)
        if ui > length(upper) || (li <= length(lower) && lower[li] <= upper[ui])
            arr[i] = lower[li]
            li += 1
        else
            arr[i] = upper[ui]
            ui += 1
        end
    end
end

"""
Swaps the values of the two indices in the array
"""
function swap(arr, i1, i2)
    tmp = arr[i1]
    arr[i1] = arr[i2]
    arr[i2] = tmp
end

"""
Perform an insertion sort on the specified Vector of Integers
"""
function insertion_sort!(arr::AbstractVector{T} where T <: Integer)
    for i in 2:length(arr)
        insertion_value = arr[i]
        insertion_index = i
        while insertion_index > 1 && insertion_value < arr[insertion_index - 1]
            insertion_index -= 1
            swap(arr, insertion_index + 1, insertion_index)
        end
        arr[insertion_index] = insertion_value
    end
end