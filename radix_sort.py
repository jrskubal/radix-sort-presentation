import random

def radix_sort(arr):
    # Convert everything into strings
    strings = [str(i) for i in arr]

    # Pad strings so they are all the same length
    max_length = max(len(s) for s in strings)
    strings = [i.rjust(max_length, '0') for i in strings]

    # Iterate through strings, grouping them by their digit
    for i in range(1, max_length + 1):
        # Initialize buckets
        buckets = [[] for i in range(10)]

        # Fill buckets
        for string in strings:
            bucket_idx = int(string[-i])
            buckets[bucket_idx].append(string)

        print(buckets)

        # Concatinate buckets
        strings = [j for i in buckets for j in i]

    # Copy result
    arr[:] = [int(i) for i in strings]


arr = [random.randint(1, 150) for _ in range(15)]
print("Initial Array:", arr)
radix_sort(arr)
print("Final Array:", arr)